# website
The intent of this project is to explore the management of a static website on AWS s3.

## Scope

1. Version Control static content
1. Create AWS resources (s3 bucket)
1. Deploy content to s3
